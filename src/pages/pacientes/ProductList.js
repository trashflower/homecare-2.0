import React, { useState, useEffect} from "react";
import MaterialTable from 'material-table';
import Typography from '@material-ui/core/Typography';
import axios from 'axios';
import Tooltip from '@material-ui/core/Tooltip';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';

const ProductList = () => {

  const baseurl = "http://localhost:8001/";

const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

const columnas = [
  {title: 'Acciones', field: 'paciente_id',render: rowData => {                                                                      
    return(
         <div>
        <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
          ...
        </Button>
        
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
        >
          <MenuItem onClick={handleClose}>Historial</MenuItem>
        </Menu>
      </div> 
    )}},
  {title: 'Paciente', field: 'paciente_nombre'},
  {title: 'Cedula',field: 'paciente_decula'},
  {title: 'Fecha nac.',field: 'paciente_fecha_nacimiento'},
  {title: 'Edad',field: 'paciente_edad'},
  {title: 'Condicion', field: 'condicion',render: rowData => {                                                                      
    return(
        rowData.id_condicion == "1" ? <div><Tooltip title={rowData.condicion}><span style={{color: '#36C95F'}}><i class="fa fa-heartbeat fa-lg"></i></span></Tooltip> {" "}{" "} </div> :
        rowData.id_condicion == "2" ? <div><Tooltip title={rowData.condicion}><span style={{color: '#ffb800'}}><i class="fa fa-heartbeat fa-lg"></i></span></Tooltip> {" "}{" "}</div> :
        rowData.id_condicion == "3" ? <div><Tooltip title={rowData.condicion}><span style={{color: '#EE3232'}}><i class="fa fa-heartbeat fa-lg"></i></span></Tooltip> </div> :
        <Tooltip title="Informacion no cargada"><span style={{color: 'lightblue'}}><i class="fa fa-heartbeat fa-lg"></i></span></Tooltip>
    )}},
  {title: 'Empresa',field: 'empresa_abreviatura'},
  {title: 'Nivel de riesgo',field: 'paciente_nivel_riesgo',render: rowData => {                                                                      
    return(
        rowData.paciente_nivel_riesgo == "Nivel 1" ? <div><span style={{color: '#36C95F'}}>{rowData.paciente_nivel_riesgo}</span> {" "}{" "} </div> :
        rowData.paciente_nivel_riesgo == "Nivel 2" ? <div><span style={{color: '#ffb800'}}>{rowData.paciente_nivel_riesgo}</span> {" "}{" "}</div> :
        rowData.paciente_nivel_riesgo == "Nivel 3" ? <div><span style={{color: '#EE3232'}}>{rowData.paciente_nivel_riesgo}</span> </div> :
         <span>-</span>
    )}}
];

  const [data, setData]= useState([]);
  const [selectedRow, setSelectedRow] = useState(null);
  let config = JSON.parse(localStorage.getItem("configuracion"))

  const peticionesGet=async()=>{
    
    fetch(baseurl+"patients/list",
    {
    	method: "GET",
      headers: {
        "api_token": config['api_token'],
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    })
    .then((response) => response.json())
    .then((responseData) => {
      setData(responseData);
      return responseData;
    })
    .catch(error => console.warn(error));
  }
 
  useEffect(()=>{
      peticionesGet();
  }, [])

  return (
    <div>
      <link
        rel="stylesheet"
        href="https://fonts.googleapis.com/icon?family=Material+Icons"
      />
      <Box textAlign="right" m={1}><Button style={{background: '#36C95F'}} variant="contained" color="primary">Nuevo</Button></Box>
      <MaterialTable
        columns={columnas}
        data={data}
        options={{
          showTitle: false, 
        }}
      />
    </div>
  );
};

export default ProductList;
