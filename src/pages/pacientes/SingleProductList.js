import React from "react";
import { Link } from "react-router-dom";

const SingleProductList = (props) => {
   const {
      previewImg,
      price,
      rating,
      availability,
      productCode,
      brand,
      des,
   } = props.product;
   return (
      <div className="col-lg-12 col-xl-12">
         <div class="card">
            <div class="card-body">
               <table class="table table-hover">
                  <thead>
                     <tr>
                        <th scope="col">Acciones</th>
                        <th scope="col">Cedula</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">fecha Nac.</th>
                        <th scope="col">Empresa</th>
                        <th scope="col">Nivel de riesgo</th>
                        <th scope="col">Condicion</th>
                        <th scope="col">Estado</th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <th scope="row">1</th>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                        <th scope="row">1</th>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                     </tr>
                     <tr>
                        <th scope="row">2</th>
                        <td>Jacob</td>
                        <td>Thornton</td>
                        <td>@fat</td>
                        <th scope="row">2</th>
                        <td>Jacob</td>
                        <td>Thornton</td>
                        <td>@fat</td>
                     </tr>
                     <tr>
                        <th scope="row">3</th>
                        <td>Larry the Bird</td>
                        <td>Larry </td>
                        <td>@twitter</td>
                        <th scope="row">3</th>
                        <td>Larry the Bird</td>
                        <td>Larry </td>
                        <td>@twitter</td>
                     </tr>
                  </tbody>
               </table>

            </div>
         </div>
      </div>
   );
};

export default SingleProductList;
