import React,{ useState, useEffect} from "react";
import { useHistory } from "react-router-dom";
import logohc from "../../images/logo-hc.png";
const Login = () => {
  const [username,setUser]=useState("");
  const [password,setPass]=useState("");
  const history = useHistory();
  useEffect(() => {
    if(localStorage.getItem('user-info')){
        history.push("/add")
    }
  }) 
  
   async function login(){
    let datos={
      "username":username,
      "password":password
    }
    let result = await fetch("http://localhost:8001/auth/login", {
      method: 'POST',
      headers:{
        "Content-Type": "application/json",
        "Accept": "application/json"
      },
      body: JSON.stringify(datos) 
    });
   result = await result.json();
    console.log(result)
    localStorage.setItem("configuracion",JSON.stringify(result))
  }
  return (
    <>
      <div>
        <div className="row d-flex justify-content-center">
          <div className="col-xl-6 col-xxl-6 col-lg-6 ">
            <div className="authincation-content">
              <div className="row no-gutters">
                <div className="col-xl-12">
                  <div className="auth-form">
                    <div className="d-flex flex-column justify-content-center align-items-center">
                      <img
                        src={logohc}
                        style={{
                          width: "60%",
                          filter: "brightness(1.1)",
                          mixBlendMode: "multiply",
                        }}
                        className="mb-4"
                      ></img>
                    </div>
                    <form action="">
                      <div className="form-group">
                        <label className="mb-1">
                          <strong>Nombre Usuario</strong>
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Usuario"
                          onChange={(e)=>setUser(e.target.value)}
                          name="name"
                        />
                      </div>
                      <div className="form-group">
                        <label className="mb-1">
                          <strong>Clave</strong>
                        </label>
                        <input
                          type="password"
                          className="form-control"
                          defaultValue=""
                          name="password"
                          onChange={(e)=>setPass(e.target.value)}
                        />
                      </div>
                      <div className="text-center mt-4">
                        <input
                          //type="submit"
                          value="Aceptar"
                          className="btn btn-primary btn-block"
                          onClick={login}
                        />
                      </div>
                    </form>
                    <div className="new-account mt-3"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default Login;
