import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { gcpAgil } from "../../services/servicios";
import { validacionLogin } from "../../helpers";
import { StorageSess } from "../../redux/actions";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
const Login = () => {
  const isAuth = useSelector((state) => state.reducerAuth.stdAuth);
  const dispatch = useDispatch();
  const history = useHistory();
  const [loginData, setLoginData] = useState({ usuario: "", clave: "" });
  const [statebtn, setbtn] = useState({
    textBtn: "Iniciar Sesion",
    isDisable: false,
  });
  const { textBtn, isDisable } = statebtn;
  const { usuario, clave } = loginData;

  const handleBlur = (e) => {
    const newLoginData = { ...loginData };
    newLoginData[e.target.name] = e.target.value;
    setLoginData(newLoginData);
  };

  const submitHandler = (e) => {
    e.preventDefault();
    if (validacionLogin(usuario, clave, toast)) {
      setbtn({
        ...statebtn,
        isDisable: true,
      });

      let formDa = new FormData();
      formDa.append("op", "dologin");
      formDa.append("usuario", usuario);
      formDa.append("clave", clave);

      gcpAgil(formDa)
        .then(function (response) {
          if (response.status) {
            if (response.data.estatus === "ok") {
              dispatch(StorageSess(response.data));
              setbtn({
                ...statebtn,
                isDisable: false,
              });
            } else {
              toast("Usuario o contraseña incorrecto", {
                position: toast.POSITION.BOTTOM_RIGHT,
                className: "foo-bar",
              });
              setbtn({
                ...statebtn,
                isDisable: false,
              });
            }
          } else {
            setbtn({
              ...statebtn,
              isDisable: false,
            });
            toast("Error en peticion https!", {
              position: toast.POSITION.BOTTOM_RIGHT,
              className: "foo-bar",
            });
          }
        })
        .catch(function (err) {
          setbtn({
            ...statebtn,
            isDisable: false,
          });

          toast("Problema con la solicitud : (", {
            position: toast.POSITION.BOTTOM_RIGHT,
            className: "foo-bar",
          });
        });
    }
  };

  useEffect(() => {
    if (isAuth) {
      history.push("/");
    }
  }, [history, isAuth]);

  return (
    <div className="authincation h-100 p-meddle">
      <div className="container h-100">
        <div className="row justify-content-center h-100 align-items-center">
          <div className="col-md-6">
            <div className="authincation-content">
              <div className="row no-gutters">
                <div className="col-xl-12">
                  <div className="auth-form">
                    <h4 className="text-center mb-4">Mercer Marsh</h4>
                    <form action="" onSubmit={(e) => submitHandler(e)}>
                      <div className="form-group">
                        <label className="mb-1">
                          <strong>Usuario</strong>
                        </label>
                        <input
                          value={usuario}
                          type="text"
                          className="form-control"
                          name="usuario"
                          onChange={handleBlur}
                        />
                      </div>
                      <div className="form-group">
                        <label className="mb-1">
                          <strong>Clave</strong>
                        </label>
                        <input
                          value={clave}
                          type="password"
                          className="form-control"
                          name="clave"
                          onChange={handleBlur}
                        />
                      </div>
                      {/*<div className="form-row d-flex justify-content-between mt-4 mb-2">
                        <div className="form-group">
                          <div className="custom-control custom-checkbox ml-1">
                            <input
                              type="checkbox"
                              className="custom-control-input"
                              id="basic_checkbox_1"
                            />
                            <label
                              className="custom-control-label"
                              htmlFor="basic_checkbox_1"
                            >
                              Remember my preference
                            </label>
                          </div>
                        </div>
                        <div className="form-group">
                          <Link to="/page-forgot-password">
                            Forgot Password?
                          </Link>
                        </div>
                      </div>*/}
                      <div className="text-center">
                        <input
                          type="submit"
                          value={!isDisable ? textBtn : "Cargando"}
                          className="btn btn-primary btn-block"
                        />
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ToastContainer />
    </div>
  );
};

export default Login;
