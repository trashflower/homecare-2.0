import React, { Fragment, useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import SideBar from "./SideBar";
import NavHader from "./NavHader";
import Header from "./Header";
import { logout } from "../../../redux/actions";

const JobieNav = ({ title, width }) => {
  console.log(width);
  const isAuth = useSelector((state) => state.reducerAuth.stdAuth);
  const [toggle, setToggle] = useState("");
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    if (isAuth === false) {
      history.push("/page-login");
    }
  }, [history, isAuth]);

  const onClick = (name) => setToggle(toggle === name ? "" : name);

  const close = () => {
    dispatch(logout());
  };

  return (
    <Fragment>
      <NavHader />
      <SideBar width={width} />
      <Header
        onNote={() => onClick("chatbox")}
        onNotification={() => onClick("notification")}
        onProfile={() => onClick("profile")}
        toggle={toggle}
        title={title}
        onBox={() => onClick("box")}
        close={close}
      />
    </Fragment>
  );
};

export default JobieNav;
