import React from "react";
import { Link } from "react-router-dom";
import PerfectScrollbar from "react-perfect-scrollbar";
import { useSelector, useDispatch } from "react-redux";
import { isNavegacion } from "../../../redux/actions";
import {
  faCartPlus,
  faHome,
  faMapMarked,
  faPumpMedical,
  faUser,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const SideBar = ({ width }) => {
  console.log(width);
  const dispatch = useDispatch();
  const isNav = useSelector((state) => state.funcionalidad.isNav);

  console.log(isNav);
  const toggle = () => {
    if (width > 1300 && isNav) return null;
    dispatch(isNavegacion(!isNav));
  };
  /*  useEffect(() => {
    let btn = document.querySelector(".nav-control");
    const aaa = document.querySelector("#main-wrapper");

    function toggleFunc() {
      return aaa.classList.toggle("menu-toggle");
    }
    btn.addEventListener("click", toggleFunc);
  });
*/
  /// Path
  let path = window.location.pathname;
  path = path.split("/");
  path = path[path.length - 1];

  /// Active menu
  let deshBoard = [
    "",
    "patient-list",
    "patient-details",
    "doctor-list",
    "doctor-details",
    "reviews",
  ];

  return (
    <div className="deznav">
      <PerfectScrollbar className="deznav-scroll">
        <div className="metismenu">
          <li className={`${deshBoard.includes(path) ? "mm-active" : ""}`}>
            <Link
              className="has-arrow ai-icon"
              to="/homecare/dashboard"
              aria-expanded="false"
              onClick={toggle}
            >
              <FontAwesomeIcon icon={faHome} style={{ color: "#444" }} />{" "}
              <span className="nav-text">Dashboard</span>
            </Link>
          </li>
          <li className={`${deshBoard.includes(path) ? "mm-active" : ""}`}>
            <Link
              className="has-arrow ai-icon"
              to="/homecare/pacientes"
              aria-expanded="false"
              onClick={toggle}
            >
              <FontAwesomeIcon icon={faUser} style={{ color: "#444" }} />
              <span className="nav-text">Pacientes</span>
            </Link>
          </li>
          <li className={`${deshBoard.includes(path) ? "mm-active" : ""}`}>
            <Link
              className="has-arrow ai-icon"
              to="/homecare/seguimiento"
              aria-expanded="false"
              onClick={toggle}
            >
              <FontAwesomeIcon icon={faMapMarked} style={{ color: "#444" }} />
              <span className="nav-text">Seguimiento</span>
            </Link>
          </li>
          <li className={`${deshBoard.includes(path) ? "mm-active" : ""}`}>
            <Link
              className="has-arrow ai-icon"
              to="/homecare/micompra"
              aria-expanded="false"
              onClick={toggle}
            >
              <FontAwesomeIcon icon={faCartPlus} style={{ color: "#444" }} />
              <span className="nav-text">Mi orden</span>
            </Link>
          </li>
        </div>
      </PerfectScrollbar>
    </div>
  );
};

export default SideBar;
