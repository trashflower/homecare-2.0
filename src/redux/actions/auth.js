export const CERRAR_S = "[AUT] CERRAR_S";
export const STORE_SAVE = "[AUT] STORE_SAVE";
export const ERROR_HTTP = "[AUT] ERROR_HTTP";
export const ERROR_AUTH = "[AUT] ERROR_AUTH";

export const SET_ACION = "[FUNCIONALIDAD] SET_ACION";
export const RESET_ACION = "[FUNCIONALIDAD] RESET_ACION";

export const MODAL = "[FUNCIONALIDAD] MODAL";
export const STOP_UPDATE = "[FUNCIONALIDAD] STOP_UPDATE";

export const StorageSess = (data) => {
  return (dispatch) => {
    const sessionStore = {
      user: data,
      stdAuth: true,
    };
    /*-CREO-EL-ARREGLO-Y-REALIZO-PUSH-DE-SESSIONSTORE*/
    let isSession = [];
    isSession.push(sessionStore);
    /*-ALMECENO-EL-ARREGLO-*/
    sessionStorage.setItem("session", JSON.stringify(isSession));
    dispatch(StoreSave(data, true));
  };
};

export const StoreSave = (data, stdAuth) => ({
  type: STORE_SAVE,
  payload: {
    data: data,
    stdAuth: stdAuth,
  },
});

export const inyeccionStore = () => {
  return (dispatch) => {
    if (sessionStorage.getItem("session") !== null) {
      /*-RECUPERO-LA-SESSION-*/
      let session = sessionStorage.getItem("session");
      /*-PARSEO-EL-ARREGLO-*/
      let parseado = JSON.parse(session);
      const [{ user, stdAuth }] = parseado;
      dispatch(StoreSave(user, stdAuth));
    } else {
      console.log("error");
    }
  };
};

export const logout = () => {
  return (dispatch) => {
    if (sessionStorage.getItem("session") !== null) {
      sessionStorage.removeItem("session");
      dispatch(logoutAuth());
    }
  };
};

const logoutAuth = () => ({
  type: CERRAR_S,
});

//FUNCIONALIDAD
export const Set_user = (data, stdAuth) => ({
  type: STORE_SAVE,
  payload: {
    data: data,
    stdAuth: stdAuth,
  },
});

