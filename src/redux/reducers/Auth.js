import { CERRAR_S, STORE_SAVE, ERROR_AUTH, ERROR_HTTP } from "../actions/auth";
let initial = {
  user: {},
  stdAuth: true,
  errorHttp: false,
  errorAuth: false,
};
/*
  userProyect: "",
  accion: "open",
  ismodal: false,
  stop: true, */
const reducerAuth = (state = initial, actions) => {
  switch (actions.type) {
    case STORE_SAVE:
      return {
        ...state,
        user: actions.payload.data,
        stdAuth: actions.payload.stdAuth,
      };
    case CERRAR_S:
      return {
        ...state,
        user: {},
        stdAuth: false,
      };
    case ERROR_AUTH:
      return {
        ...state,
        errorAuth: actions.payload.errorAuth,
        stdAuth: actions.payload.stdAuth,
      };
    case ERROR_HTTP:
      return {
        ...state,
        errorHttp: actions.payload.errorHttp,
        msg: actions.payload.msg,
      };
    /* case STOP_UPDATE:
      return {
        ...state,
        stop: actions.payload,
      };*/
    default:
      return state;
  }
};
export default reducerAuth;
