import {
  AppContext,
  ContextProvider,
  ContextConsumer,
  openNav,
  closeNav,
  hideNav,
  openFile,
  saveFile,
  trimString,
  userVisitedBefore,
  updateFile,
  validaFile,
  isExtension,
} from "./utils";

import { ContextProviderWrapper } from "./ContextProviderWrapper";

export {
  AppContext,
  ContextProvider,
  ContextConsumer,
  openNav,
  closeNav,
  hideNav,
  openFile,
  saveFile,
  trimString,
  ContextProviderWrapper,
  userVisitedBefore,
  updateFile,
  validaFile,
  isExtension,
};
