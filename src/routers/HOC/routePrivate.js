import React from "react";
import { Redirect, Route } from "react-router-dom";
import PropTypes from "prop-types";

export const RoutePrivate = ({ stdAuth, component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(prop) =>
        stdAuth === true ? (
          <Component {...prop} />
        ) : (
          <Redirect from="/" to="/page-login" />
        )
      }
    />
  );
};
RoutePrivate.protoTypes = {
  stdAuth: PropTypes.bool.isRequired,
  component: PropTypes.func.isRequired,
};
