import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import Dashboard from "../pages/dashboard/dashboard";
import ProductList from "../pages/pacientes/ProductList";
import {} from "../pages/productDetails/ProductDetail";
import Checkout from "../pages/shoping/Checkout";
import Register from "../pages/suscripcion/Registration";
import Invoice from "../pages/Invoice/Invoice";
import Maps from "../pages/maps/maps";
//import BoardContainer from "../components/board/BoardContainer";
const PrivateRouter = () => {
  return (
    <>
      <Switch>
        <Route exact path="/homecare/dashboard" component={Dashboard} />
        <Route exact path="/homecare/pacientes" component={ProductList} />
        <Route exact path="/homecare/pacientes" component={ProductList} />
        <Route exact path="/homecare/micompra" component={Checkout} />
        <Route exact path="/homecare/login" component={Register} />
        <Route exact path="/homecare/detallecompra" component={Invoice} />
        <Route exact path="/homecare/seguimiento" component={Maps} />
        {/* <Route path="/gcpagil/page-b/:boardId" component={BoardContainer} />
         */}
        <Route
          exact
          path="/"
          render={() => <Redirect to="/homecare/login" />}
        />
      </Switch>
    </>
  );
};
export default PrivateRouter;
