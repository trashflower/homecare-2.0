import { useState, useEffect } from "react";
import { servicesBmc, servicesGcp } from "../services/servicios";

export function useFetch(url, action, page, servicio = "bmc", reload = "") {
  let servicios = servicio === "bmc" ? servicesBmc : servicesGcp;
  const [load, setLoad] = useState(true);
  const [data, setData] = useState([]);
  const [totalResults, settotalResults] = useState(0);

  function concatState(response) {
    setData((prev) => [...prev, response]);
  }

  function deletState(id) {
    let nuevo = data.filter((item) => item.id !== id);
    setData(nuevo);
  }
  const updateState = (item) => {
    let nuevo = data.map((items) =>
      items.id === item.id ? (items = item) : items
    );
    setData(nuevo);
  };
  useEffect(() => {
    servicios
      .get(url, {
        params: {
          op: action,
          page: page,
        },
        responseType: "json",
      })
      .then((rsp) => {
        if (rsp.data.estatus === "ok") {
          if (rsp.data) {
            setLoad(false);
            setData(rsp.data.data);
            settotalResults(rsp.data.totalResults);
          }
        }
      })
      .catch(() => {});
  }, [url, servicios, action, page, reload]);
  return [data, load, totalResults, concatState, deletState, updateState];
}
