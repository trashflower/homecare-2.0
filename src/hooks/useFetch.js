import { useState, useEffect } from "react";
import { getCards, getLists, getBoards } from "../services/servicios";

export function useFetchAll(url, action, page) {
  const [load, setLoad] = useState(true);
  const [dataBoards, setDataBoards] = useState([]);
  const [dataList, setDataList] = useState([]);
  const [dataCards, setDataCards] = useState([]);
  const [totalResults, settotalResults] = useState({
    board: 0,
    list: 0,
    card: 0,
  });
  const [error, setError] = useState("");
  useEffect(() => {
    Promise.all([getCards(), getLists(), getBoards()])
      .then((rsp) => {
        const [card, lits, board] = rsp;
        if (board.data.data) setDataBoards(board.data.data);
        if (lits.data.data) setDataList(lits.data.data);
        if (card.data.data) setDataCards(card.data.data);
        settotalResults({
          board: board.data.totalResults,
          list: lits.data.totalResults,
          card: card.data.totalResults,
        });
        setLoad(false);
      })
      .catch((error) => {
        setDataBoards([]);
        setDataList([]);
        setDataCards([]);
        setError(error);
        setLoad(false);
      });
  }, [url, action, page]);
  return [dataBoards, dataList, dataCards, load, totalResults, error];
}
