import { useState, useEffect } from "react";
import { servicesBmc } from "../services/servicios";

export function useFetching(url, action, page, id, reload) {
  const [load, setLoad] = useState(true);
  const [data, setData] = useState([]);
  const [totalResults, settotalResults] = useState(0);

  function concatState(response) {
    setData((prev) => [...prev, response]);
  }

  function deletState(id) {
    let nuevo = data.filter((item) => item.id !== id);
    setData(nuevo);
  }
  const updateState = (item) => {
    let x = data.map((items) =>
      items.id === item.id ? (items = item) : items
    );
    setData(x);
  };
  const updateProgreso = (item) => {
    let x = data.map((items) =>
      items.id === item.id ? (items = item) : items
    );
    setData(x);
  };
  useEffect(() => {
    servicesBmc
      .get(url, {
        params: {
          op: action,
          page: page,
          id: id,
        },
        responseType: "json",
      })
      .then((rsp) => {
        if (rsp.data.estatus === "ok") {
          if (rsp.data.data) {
            setLoad(false);
            setData(rsp.data.data);
            settotalResults(rsp.data.totalResults);
          } else {
            setData([]);
            setLoad(false);
          }
        }
      })
      .catch(() => {});
  }, [url, action, page, id, reload]);
  return [
    data,
    load,
    concatState,
    deletState,
    totalResults,
    updateState,
    updateProgreso,
  ];
}
